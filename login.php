<?php
session_start();

//traite le formulaire tout en haut du fichier

//initialise le tableau d'éventuelles erreurs
$errors = [];

//la variable $_POST contient les données du form s'il est soumis, ou un tableau vide sinon
//var_dump($_POST);

include('inc/db.php');

//est-ce que le formulaire est soumis ?
if (!empty($_POST)){
    //il n'est pas vide, donc le form est soumis...

    //récupère nos données dans nos propres variables
    //le strip_tags() permet de retirer les balises HTML (si qqn essaie de nous faire une attaque XSS)
    $login = strip_tags($_POST['login']);
    $password = $_POST['password'];

    //validation des données

    //l'login est requis... est-il vide ?
    if (empty($login)){
        $errors['login'] = "Veuillez saisir votre pseudo ou votre email !";
    }

    //validation du mot de passe
    /*if(mb_strlen($password) < 12){
        $errors['password'] = "Votre mot de passe devrait avoir au moins 12 caractères !";
    }*/
    //regex pour minimum 12 caractères et une lettre et un chiffre piquée ici
    //https://stackoverflow.com/questions/19605150/regex-for-password-must-contain-at-least-eight-characters-at-least-one-number-a
    $regex = "/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{12,}$/";
    if(!preg_match($regex, $password)){
        $errors['password'] = "Votre mot de passe devrait avoir au moins 12 caractères et contenir une lettre et un chiffre !";
    }

    //si tout est valide...
    if (empty($errors)){
        //todo: à suivre

        $foundUser = getUserByEmailOrUsername($login);
        //si l'email ou le pseudo existe...
        if ($foundUser){
            //mot de passe ok ?
            $isPasswordValid = password_verify($password, $foundUser['password']);
            if ($isPasswordValid){
                //connexion !
                //on stocke les données du user dans la session
                $_SESSION['user'] = $foundUser;

                //redirige vers une autre page
                header("Location: index.php");
                //on arrête l'exécution du script ici (pour être sûr que le reste du code ci-dessous ne soit pas exécuté)
                die();
            }
            else {
                $errors['login'] = 'Mauvais identifiants !';
            }
        }
        else {
            $errors['login'] = 'Mauvais identifiants !';
        }
    }
    if (!empty($errors)) {
        $_SESSION["flash"] = ["Les identifiants sont inccorects. Veuillez corriger SVP", "danger"];
    }
}

//le haut de notre html
//pour éviter la répétition de ce code sur toutes les pages
include("inc/top.php");

?>

    <main class="section">
        <div class="container">
            <div class="content">
                <h2 class="title is-3">Re !</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Commodi dicta dolor fugit harum id nemo numquam similique sunt! Accusantium aperiam asperiores culpa ea eum magnam molestias officiis placeat rem repellendus!</p>
            </div>
            <div class="columns">
                <div class="column is-three-fifths">
                    <div class="box">
                        <h2 class="title is-4">Connectez-vous !</h2>

                        <!-- le novalidate désactive la validation HTML5 qui nous embête pour tester nos validations PHP -->
                        <form method="post" novalidate="novalidate">
                            <div class="field">
                                <label for="login_input">Votre pseudo ou votre login</label>
                                <div class="control">
                                    <input type="text" value="<?= isset($login) ? $login : "" ?>" class="input <?= !empty($errors['login']) ? "is-danger" : "" ?>" id="login_input" name="login" placeholder="yo@gmail.com">
                                </div>
                                <?php if(!empty($errors['login'])): ?>
                                    <p class="help is-danger"><?= $errors['login'] ?></p>
                                <?php endif; ?>
                            </div>

                            <div class="field">
                                <label for="password_input">Votre mot de passe</label>
                                <div class="control">
                                    <input type="password" class="input <?= !empty($errors['password']) ? "is-danger" : "" ?>"
                                           id="password_input" name="password">
                                </div>
                                <?php if(!empty($errors['password'])): ?>
                                    <p class="help is-danger"><?= $errors['password'] ?></p>
                                <?php endif; ?>
                            </div>

                            <div class="field">
                                <div class="control">
                                    <button class="button is-success is-light">Connexion</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

                <div class="column">
                    <div class="content">
                        <h3>Pas de compte ?</h3>
                        <p><a href="inscription.php" class="button is-success is-light">Créez-le ici !</a></p>
                    </div>
                </div>
            </div>
        </div>
    </main>

<?php

//le bas de notre html
include("inc/bottom.php");

?>