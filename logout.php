<?php

session_start();

unset($_SESSION['user']);

$_SESSION['flash'] = ["Vous êtes déconnecté.", "success"];

header("Location: login.php");
die();