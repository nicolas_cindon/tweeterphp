<footer class="footer">
    <div class="container">
        <nav class="menu columns">
            <div class="column is-two-fifth">
                <p class="menu-label">
                    Général
                </p>
                <ul class="menu-list">
                    <li><a href="index.php">Accueil</a></li>
                    <?php if(isset($_SESSION['user'])) : ?>
                        <li><a href="tweet.php">Poster un message</a></li>
                    <?php endif; ?>
                </ul>
            </div>
            <div class="column is-two-fifth">
                <p class="menu-label">
                    Comptes
                </p>
                <ul class="menu-list">
                    <?php if(!isset($_SESSION['user'])) : ?>
                    <li><a href="inscription.php">Inscription</a></li>
                    <li><a href="login.php">Connexion</a></li>
                    <?php else : ?>
                    <li><a href="logout.php">Déconnexion</a></li>
                    <li><a href="#">Supprimer mon compte</a></li>
                    <?php endif; ?>
                </ul>
            </div>
            <div class="column is-two-fifth">
                <p class="menu-label">
                    Informations légales
                </p>
                <ul class="menu-list">
                    <li><a href="#">RGPD</a></li>
                    <li><a href="http://opensource.org/licenses/mit-license.php" target="_blank">The source code is licensed</a></li>
                    <li><a href="http://creativecommons.org/licenses/by-nc-sa/4.0/" target="_blank">The website content is licensed</a></li>
                </ul>
            </div>
        </nav>

        <div class="content has-text-centered">
            <p>
                <strong>Tweeter</strong> &copy; <?= date('Y')?> by <a href="https://www.eni-ecole.fr" target="_blank">ENI Ecole Informatique</a>.
            </p>
            <!--<p>Pages vues : <?php //echo $_SESSION['pageViews'] ?></p>-->
        </div>
    </div>
</footer>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script>
    $(document).ready(function() {

        // Check for click events on the navbar burger icon
        $(".navbar-burger").click(function() {

            // Toggle the "is-active" class on both the "navbar-burger" and the "navbar-menu"
            $(".navbar-burger").toggleClass("is-active");
            $(".navbar-menu").toggleClass("is-active");

        });
    });
</script>
</body>
</html>